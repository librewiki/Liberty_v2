import os

from liberty import app
from liberty.base import engine
from liberty.models import *


@app.cli.command("init_db")
def create_all():
    Base.metadata.create_all(bind=engine)


@app.cli.command("drop_db")
def drop_all():
    Base.metadata.drop_all(bind=engine)


@app.cli.command("reset_db")
def reset():
    drop_all()
    create_all()


@app.cli.command("extract_i18n")
def extract_i18n():
    with os.popen("pybabel extract -F confs/babel.cfg -o messages.pot .") as p:
        for l in p.readlines():
            print(l)


@app.cli.command("init_i18n")
def init_i18n():
    if not os.path.exists("translations"):
        os.mkdir("translations")
    with os.popen("pybabel init -i messages.pot -d translations -l ko") as p:
        for l in p.readlines():
            print(l)
    with os.popen("pybabel init -i messages.pot -d translations -l ja") as p:
        for l in p.readlines():
            print(l)
    with os.popen("pybabel init -i messages.pot -d translations -l de") as p:
        for l in p.readlines():
            print(l)


@app.cli.command("update_i18n")
def update_i18n():
    with os.popen("pybabel update -i messages.pot -d translations") as p:
        for l in p.readlines():
            print(l)


@app.cli.command("compile_i18n")
def compile_i18n():
    with os.popen("pybabel compile -d translations") as p:
        for l in p.readlines():
            print(l)
