CACHE_TYPE = 'simple' # simple, redis, memcached
CACHE_KEY_PREFIX = 'LIBERTY_'
CACHE_TTL = 600
# SERVER_NAME="liberty-wiki.com"
DEBUG = True
TESTING = False
SECRET_KEY = "ThisIsNotSecure"
DB_TYPE = "sqlite" # sqlite, mysql, postgresql, oracle
DB_ADDR = "dev.db"
DB_PORT = "" # empty if using sqlite
DB_USER = "" # empty if using sqlite
DB_PASSWD = "" # empty if using sqlite
DB_NAME = "" # empty if using sqlite
BABEL_DEFAULT_LOCALE = 'ko'
BABEL_DEFAULT_TIMEZONE = 'Asia/Seoul'
DB_OPTIONS = dict(
    echo=False,
)
ENABLED_MODULES = [
]