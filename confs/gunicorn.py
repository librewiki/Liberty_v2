import multiprocessing

bind = "unix:/var/tmp/liberty.sock"
bind = "127.0.0.1:4020"
workers = multiprocessing.cpu_count() * 2 + 1
worker_class = 'gunicorn.workers.gthread.ThreadWorker'
threads = multiprocessing.cpu_count() * 2 + 1
max_requests = 10000
max_requests_jitter = 1000
# pid = '/var/tmp/liberty.pid'
# user = 'liberty'
# group = 'liberty'
proc_name = 'liberty'