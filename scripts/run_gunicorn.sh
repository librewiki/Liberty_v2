#!/bin/bash

export PATH="`pwd`/env/bin:$PATH"
export PYTHONPATH=`pwd`
export CONFIGPATH="`pwd`/confs/config.py"


echo $PATH
echo $PYTHONPATH
echo $CONFIGPATH

gunicorn -c `pwd`/confs/gunicorn.py liberty:app
