import os

from flask import Flask, g
from flask_caching import Cache
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from werkzeug.contrib.fixers import ProxyFix
from flask_wtf.csrf import CsrfProtect
from flask_oauthlib.client import OAuth
from flask_babel import Babel, gettext, ngettext

from liberty.helper.json import CustomJSONEncoder


config = os.environ.get('CONFIGPATH')
if not config:
    config = os.path.join(os.path.dirname(__file__), '..', 'confs', 'config.py')

app = Flask(__name__)

app.json_encoder = CustomJSONEncoder

app.config.from_pyfile(config)
app.url_map.charset = 'utf-8'
app.jinja_env.auto_reload = app.debug
app.jinja_env.trim_blocks = not app.debug
app.jinja_env.lstrip_blocks = not app.debug
app.wsgi_app = ProxyFix(app.wsgi_app)

print(app.config)

cache = Cache(app)
csrf = CsrfProtect(app)
oauth = OAuth(app)
babel = Babel(app)

db_string = ""
if app.config['DB_TYPE'] == 'sqlite':
    db_string = "{}:///{}".format(app.config['DB_TYPE'], app.config['DB_ADDR'])
else:
    db_string = "{}://{}:{}@{}:{}/{}".format(app.config['DB_TYPE'], app.config['DB_USER'], app.config['DB_PASSWD'].
                                             app.config['DB_ADDR'], app.config['DB_PORT'], app.config['DB_NAME'])
engine = create_engine(db_string, **app.config['DB_OPTIONS'])
db = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))
