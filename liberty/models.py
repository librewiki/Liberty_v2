import random
import string
import time
from json import dumps, loads
from base64 import b32encode
from datetime import datetime

import sqlalchemy as sa
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy.orm import relationship, backref
from pyotp import TOTP
from bcrypt import hashpw, gensalt

from liberty.base import db, babel, gettext, ngettext
from liberty.helper.json import CustomJSONEncoder


class DefaultBase:
    @declared_attr
    def __tablename__(self):
        return self.__name__

    __table_args__ = {}

    @property
    def dict(self):
        dic = self.__dict__
        del dic['_sa_instance_state']
        return dic

    @property
    def json(self):
        return dumps(self.dict, cls=CustomJSONEncoder)

    def distinct_count(self, property, column):
        count = db.query(sa.func.count(sa.distinct(column))).with_parent(self, property).scalar()
        if count is None:
            count = 0
        return count

Base = declarative_base(cls=DefaultBase)
Base.query = db.query_property()


class SlugComparator(sa.Unicode.Comparator):
    # TODO: 다른 연산자 지원

    def __eq__(self, other):
        return sa.Unicode.Comparator.__eq__(sa.func.lower(self.expr), sa.func.lower(other))

    def __ne__(self, other):
        return sa.Unicode.Comparator.__ne__(sa.func.lower(self.expr), sa.func.lower(other))


class JsonEncodedDict(sa.TypeDecorator):
    """Enables JSON storage by encoding and decoding on the fly."""
    impl = sa.Text

    def process_bind_param(self, value, dialect):
        return dumps(value)

    def process_result_value(self, value, dialect):
        if value:
            return loads(value)
        else:
            return None


class JsonEncodedList(sa.TypeDecorator):
    """Enables JSON storage by encoding and decoding on the fly."""
    impl = sa.Text

    def process_bind_param(self, value, dialect):
        return dumps(value)

    def process_result_value(self, value, dialect):
        if value:
            return loads(value)
        else:
            return None


class TimeStamp(sa.TypeDecorator):
    impl = sa.BigInteger

    def process_bind_param(self, value, dialect):
        if isinstance(value, datetime):
            return time.mktime(value.timetuple())
        else:
            raise TypeError(gettext("Accept datetime instance only!"))

    def process_result_value(self, value, dialect):
        return datetime.fromtimestamp(value)

MutableDict.associate_with(JsonEncodedDict)


class User(Base):
    uid = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    user_name = sa.Column(sa.String(255), nullable=False, index=True, unique=True)
    email = sa.Column(sa.String(255), nullable=False, index=True, unique=True)
    oauth_uids = sa.Column(JsonEncodedDict)
    password = sa.Column(sa.Text)
    two_fa_secret = sa.Column(sa.Text)
    two_fa_uri = sa.Column(sa.Text)
    two_fa_emerge = sa.Column(JsonEncodedList)
    two_fa_enabled = sa.Column(sa.Boolean, default=False)
    java_script = sa.Column(sa.Text)
    css = sa.Column(sa.Text)
    joined = sa.Column(TimeStamp)

    def __init__(self, user_name, email):
        self.user_name = user_name
        self.email = email
        secret = b32encode(datetime.now().strftime("%s").encode())
        totp = TOTP(secret)
        self.two_fa_secret = secret.decode()
        self.two_fa_uri = totp.provisioning_uri(email, user_name)
        emerge = []
        for i in range(6):
            emerge.append(''.join((random.choice(string.ascii_letters + string.digits)) for _ in range(20)))
        self.two_fa_emerge = dumps(emerge)
        self.joined = datetime.utcnow()

    @staticmethod
    def new_user(user_name, email, password):
        if user_name.count("@"):
            return None, gettext("User name is can not contains '@'")
        user = User(user_name, email)
        if not password:
            return None, gettext("Password is can not be null")
        user.password = hashpw(password.encode(), gensalt()).decode()
        try:
            db.add(user)
            db.commit()
            return user, None
        except Exception as e:
            db.rollback()
            return None, e

    @staticmethod
    def oauth(user_name, email, oauth, uid):
        """returns User record instance and login result message.
        This is a login method for oauth account users.
        check email is in DB records, check oauth provider and uid is match with record.
                if email is in DB record and oauth provider and uid is match, return User record instance and "OK".
                else call User.new_oauth_user() and return that returns
        :param user_name: user_name for User.new_oauth_user()
        :type user_name: str
        :param email: user email
        :type email: str
        :param oauth: user oauth provider
        :type oauth: str
        :param uid: user oauth uid
        :type uid: str
        :returns: User record instance, message
        :rtype: User, str
        """
        pass

    @staticmethod
    def new_oauth_user(user_name, email, oauth, uid):
        """returns User record instance and login result message.
        This is a join method for oauth account users.
        Create User instance, and assign values.
        and INSERT to DB.
        if error, return None and error message.
        else return User record instance and "OK"
        :param user_name: user_name for User.new_oauth_user()
        :type user_name: str
        :param email: user email
        :type email: str
        :param oauth: user oauth provider
        :type oauth: str
        :param uid: user oauth uid
        :type uid: str
        :returns: User record instance, message
        :rtype: User, str
        """
        pass

    @staticmethod
    def login(id_, password):
        """returns User instance and login result message.
        This is a login method for local account users.
        check id_ is email or user_name, get User record.
        if User record is exist, check password and sotored password is match.
          User record is exist and password is matched, return User record instance and "OK"
          else return None and error message.
        :param id_: user_name or email
        :param password: user password
        :type id_: str
        :type password: str
        :returns: user, message
        :rtype: User, str
        """
        if id_ and password:
            if id_.count("@") > 0:
                user = User.query.filter_by(email=id_).first()
            else:
                user = User.query.filter_by(user_name=id_).first()
            if user:
                if hashpw(password.encode(), user.password.encode()) == user.password.encode():
                    return user, None
            return None, gettext("No such user.")
        return None, gettext("ID and Password needed")

class Article(Base):
    """
    Article table.
    """
    uid = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    title = sa.Column(sa.Text, nullable=False, index=True)
    text = sa.Column(sa.Text)
    updated = sa.Column(TimeStamp)
    author_uid = sa.Column(sa.ForeignKey("User.uid"))

    def __init__(self, title, text, author_uid):
        self.title = title
        self.text = text
        self.author_uid = author_uid
        self.updated = datetime.now()

    @staticmethod
    def get_latest_article(title):
        return Article.query.filter_by(title=title).order_by(sa.desc(Article.uid)).first()

    @staticmethod
    def new_article(title, text, author_uid):
        if title and text and author_uid:
            article = Article(title, text, author_uid)
            try:
                db.add(article)
                db.commit()
                return article, None
            except Exception as e:
                db.rollback()
                return None, e
        return None, gettext("Title and text and user is can not be null")
