import flask as fl
from itsdangerous import BadSignature, SignatureExpired
from liberty.base import db, csrf, cache
from liberty.models import *

bp = fl.Blueprint('user', __name__, url_prefix='/user')
csrf.exempt(bp)


def init(app):
    app.register_blueprint(bp)


@bp.route("/login", methods=['POST', 'GET'])
def login():
    """ Join page.
    This page is accecpt JSON BODY only.
     {"id_": "user_name or user's email address", "password": "p@ssWord"}
    :return: 500
     :rtype: str {"error": "error description"}
    :return: 200
    :rtype: str {"error": null, "user_uid": 0}
    """
    data = fl.request.get_json()
    user, error = User.login(data.get('id_'), data.get('password'))
    if error:
        return fl.jsonify(error=str(error)), 500
    else:
        fl.session['uid'] = user.uid
        fl.session['email'] = user.email
        return fl.jsonify(error=None, user_uid=user.uid)


@bp.route("/join", methods=['POST'])
def join():
    """ Join page.
    This page is accecpt JSON BODY only.
     {"user_name": "user", "email": "user@example.com", "password": "p@ssWord"}
    :return: 500
     :rtype: str {"error": "error description"}
    :return: 200
    :rtype: str {"error": null, "user_uid": 0}
    """
    data = fl.request.get_json()
    user, error = User.new_user(data.get('user_name'), data.get('email'), data.get('password'))
    if error:
        return fl.jsonify(error=str(error)), 500
    else:
        return fl.jsonify(error=None, user_uid=user.uid)
