import flask as fl
from flask import request, json
from itsdangerous import BadSignature, SignatureExpired
from liberty.base import db, csrf, cache

bp = fl.Blueprint('sample', __name__, url_prefix='/sample')
csrf.exempt(bp)


def init(app):
    app.register_blueprint(bp)


@bp.route("/sample", methods=['POST', 'GET'])
def sample():
    pass
