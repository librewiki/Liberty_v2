import flask as fl
from flask import request, json
from itsdangerous import BadSignature, SignatureExpired
from liberty.base import db, csrf, cache, oauth


bp = fl.Blueprint('oauth', __name__, url_prefix='/oauth')
csrf.exempt(bp)


def init(app):
    app.register_blueprint(bp)


@bp.route("/sample", methods=['POST', 'GET'])
def sample():
    pass
