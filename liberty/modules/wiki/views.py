from json import dumps

import flask as fl
from itsdangerous import BadSignature, SignatureExpired
from liberty.base import db, csrf, cache
from liberty.models import *
from liberty.helper.json import jsonify

bp = fl.Blueprint('wiki', __name__, url_prefix='')
csrf.exempt(bp)


def init(app):
    app.register_blueprint(bp)


@bp.route("/")
@bp.route("/<title>")
def article(title=None):
    print("title", title)
    if not title:
        title = "index"
    rv = cache.get(title)
    if not rv:
        art = Article.get_latest_article(title)
        if art:
            rv = jsonify(art.dict)
            cache.set(title, rv)
        else:
            fl.abort(404)
    print(rv)
    resp = fl.make_response(rv)
    resp.headers['Content-Type'] = "application/json; charset:utf8"
    return resp


@bp.route("/write/", methods=['POST'])
@bp.route('/write/<title>', methods=['POST'])
def write_article(title=None):
    """
    {"title": "article title", "text": "wiki text"}
    :param title:
    :return:
    """
    if not title:
        title = "index"
    data = fl.request.get_json()
    # title, text, author_uid
    art, err = Article.new_article(title, data['text'], fl.session.get('uid'))
    if err:
        return fl.jsonify(error=str(err)), 500
    else:
        return fl.jsonify(error=None, article_uid=art.uid, title=title), 200


@bp.route('/fefe')
def fefe():
    return "ASD"