import importlib
import random
import string

import flask

from liberty.base import app, db, babel, g


__all__ = [
    'user',
    'wiki',
    'oauth'
]
if app.config.get('ENABLED_MODULES'):
    __all__.extend(app.config.get('ENABLED_MODULES'))

print(__all__)

for module in __all__:
    try:
        importlib.import_module('.modules.' + module + '.models', package=__name__)
    except ImportError:
        pass

for module in __all__:
    importlib.import_module('.modules.{}.views'.format(module), package=__name__).init(app)


@app.errorhandler(403)
def forbidden(e):
    return '<img src="https://http.cat/403.jpg" />', 403


@app.errorhandler(404)
def page_not_found(e):
    return '<img src="https://http.cat/404.jpg" />', 404


@app.errorhandler(500)
def internal_server_error(e):
    return '<img src="https://http.cat/500.jpg" />', 500


@app.before_request
def csrf_protect():
    print(flask.session)
    print(flask.request.url)
    # if flask.request.method == "POST":
    #     token = flask.session.pop('_csrf_token', None)
    #     if not token or token != flask.request.form.get('_csrf_token'):
    #         flask.abort(403)
    # elif flask.request.method == "GET":
    #     generate_csrf_token()

def generate_csrf_token():
    if '_csrf_token' not in flask.session:
        flask.session['_csrf_token'] = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(30))
    return flask.session['_csrf_token']


@babel.localeselector
def get_locale():
    locale = flask.request.accept_languages.best_match(['ko', 'ja', 'en'])
    return locale


print(app.url_map)
