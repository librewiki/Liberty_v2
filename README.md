# Liberty Engine

Liberty Engine is a wikiwiki engine inspired by MediaWiki.

## Installation

### requirements
- python >3.4
- virtualenv
- sqlite3 or postgresql or mariaDB(or MySQL)

```bash
git clone https://gitlab.com/librewiki/Liberty_v2.git $CODE_DIR
cd $CODE_DIR/
virtualenv env
. env/bin/activate
pip install -r requirements.txt
sudo sh script/install.sh (or sudo fish script/install.fish) 
sudo systemctl start liberty.service
```